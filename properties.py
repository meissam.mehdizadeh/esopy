import configparser as cp
import os.path
import pathlib

from selenium import webdriver


def get_browser():
    return get_property('Browser')


def get_testdata_uid():
    return get_property('testdata_uid')


def get_os():
    return get_property('OS')


def get_driver():
    browser = 'Firefox'  # default browser
    if get_browser() != None:
        browser = get_browser()

    os = get_os()
    os = os.lower().strip()
    browser = browser.lower().strip()

    webdriver_path = str(pathlib.Path(__file__).parent.absolute()) + "/webdriver/" + os

    if browser == 'firefox':
        driver = webdriver.Firefox(executable_path=webdriver_path + "/geckodriver")
    elif browser == 'chrome':
        driver = webdriver.Chrome(executable_path=webdriver_path + "/chromedriver")

    return driver

def get_weburl():
    return get_property('web_url')

def get_apiurl():
    return get_property('api_url')

def get_api_username():
    return get_property('api_username')

def get_api_password():
    return get_property('api_password')

def get_property(key=None):

    config_file = './config/config.ini'
    config_file = str(pathlib.Path(__file__).parent.absolute()) + '/config/config.ini'

    if os.path.isfile(config_file):
        config = cp.ConfigParser()
        config.read(config_file)

        config_selection = 'Configuration'

        if config.has_section(config_selection):
            if config.has_option(config_selection, key) and config.has_option(config_selection, key) != '':
                return config.get(config_selection, key)
            else:
                print('config key {0} not found!'.format(key))
                return None
        else:
            print('config selection {0} not found!'.format(config_selection))
            return None

    else:
        print('Config file {0} not found'.format(config_file))
        return None
