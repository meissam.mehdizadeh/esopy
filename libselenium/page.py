from selenium.common.exceptions import NoSuchElementException
from libselenium.element import BasePageElement
from libselenium.locators import MainPageLocators
from selenium.webdriver.common.action_chains import ActionChains
import time

class SearchTextElement(BasePageElement):
    """This class gets the search text from the specified locator"""

    #The locator for search box where search string is entered
    # locator = 'q'
    locator = 'search-input-mous'


class BasePage(object):
    """Base class to initialize the base page that will be called from all pages"""

    def __init__(self, driver):
        self.driver = driver


class MainPage(BasePage):
    """Home page action methods come here. I.e. Python.org"""

    time.sleep(5)

    #Declares a variable that will contain the retrieved text
    search_text_element = SearchTextElement()

    def is_title_matches(self):
        """Verifies that the hardcoded text "Python" appears in page title"""
        return "Python" in self.driver.title

    def hover_to_search_menu(self):
        time.sleep(5)

        element_search_menu = self.driver.find_element(*MainPageLocators.SEARCH_MENU)
        hover = ActionChains(self.driver).move_to_element(element_search_menu)
        hover.perform()

    def click_go_button(self):
        """Triggers the search"""
        element = self.driver.find_element(*MainPageLocators.GO_BUTTON)
        element.click()

    def check_element_available(self, search_id = None):
        try:
            element = self.driver.find_element(*MainPageLocators.SEARCH_RESULT_FIRST_ROW)

            time.sleep(5)
            found_element = element.find_elements_by_xpath("//*[contains(text(), '" + search_id + "')]")

            if len(found_element) == 0:
                return False;

        except NoSuchElementException:
            return False
        return True


class SearchResultsPage(BasePage):
    """Search results page action methods come here"""

    def is_results_found(self):
        # Probably should search for this text in the specific page
        # element, but as for now it works fine
        return "No results found." not in self.driver.page_source