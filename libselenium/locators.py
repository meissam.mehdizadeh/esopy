from selenium.webdriver.common.by import By

class MainPageLocators(object):
    """A class for main page locators. All main page locators should come here"""
    GO_BUTTON = (By.ID, 'submit')

    SEARCH_MENU = (By.ID, 'search-button')

    SEARCH_MEMBER_OUS_ID = (By.ID, 'search-input-mous')

    SEARCH_RESULT_FIRST_ROW_ELEMENT = (By.XPATH, '//*[@id="observation-panel"]/app-table-container/app-table/ngx-datatable/div/datatable-body/datatable-selection/datatable-scroller/datatable-row-wrapper/datatable-body-row/div[2]')

    SEARCH_RESULT_FIRST_ROW = (By.XPATH, '//*[@id="observation-panel"]/app-table-container/app-table/ngx-datatable/div/datatable-body/datatable-selection/datatable-scroller')

    SEARCH_RESULT_TABLE_MEMEBER_OUS_ID = (By.XPATH, '//*[@id="observation-panel"]/app-table-container/app-table/ngx-datatable/div/datatable-body/datatable-selection/datatable-scroller/datatable-row-wrapper/datatable-body-row/div[2]/datatable-body-cell[31]/div/app-table-cell-container/div/div')

class SearchResultsPageLocators(object):
    """A class for search results locators. All search results locators should come here"""
    pass