import requests
import properties as prop
import api_helper as api

SERVICE_LOGIN = '/copdemo/api/login'
SERVICE_CONTAINER = '/copdemo/api/v1/containers/1448455/items'
SERVOCE_OBS_BLICK = '/copdemo/api/v1/obsBlocks/'


class Test_API():

    def test_s_login(self):

        # Login Request
        user = prop.get_api_username()
        pw = prop.get_api_password()
        payload = {'username': user, 'password': pw}

        url = prop.get_apiurl() + SERVICE_LOGIN

        resp = requests.post(url, data=payload)

        api.print_response(resp)

        # Checks if Login was successful
        assert resp.status_code == 200, "Login failed. Response Status Code: " + str(resp.status_code)

    def test_f_login_wrong_credential(self):

        # Login Request
        user = prop.get_api_username()
        pw = 'wrongpassword'
        payload = {'username': user, 'password': pw}

        url = prop.get_apiurl() + SERVICE_LOGIN

        resp = requests.post(url, data=payload)
        api.print_response(resp)

        # Checks if Login was failed as expected
        assert resp.status_code == 401, "Login did not fail as expected. Response Status Code: " + str(resp.status_code)

    def test_s_create_and_delete_item(self):

        # Login Request
        user = prop.get_api_username()
        pw = prop.get_api_password()
        payload = {'username': user, 'password': pw}

        url = prop.get_apiurl() + SERVICE_LOGIN

        resp = requests.post(url, data=payload)
        api.print_response(resp)

        # Checks if Login was successful
        assert resp.status_code == 200, "Login failed. Response Status Code: " + str(resp.status_code)

        dic_json_resp = resp.json()

        token = dic_json_resp['access_token']

        # Create New Item : POST
        payload = {'itemType': 'OB', 'name': 'test for delete finding chart'}

        url = prop.get_apiurl() + SERVICE_CONTAINER

        header = {'Authorization': 'Bearer ' + token}

        resp = requests.post(url, json=payload, headers=header)
        api.print_response(resp)

        assert resp.status_code == 200, "Creating new Item failed. Response Status Code: " + str(resp.status_code)

        dic_json_resp = resp.json()

        obId = dic_json_resp['obId']

        # GET: Check if the OB exists

        url = prop.get_apiurl() + SERVOCE_OBS_BLICK + str(obId)

        header = {'Authorization': 'Bearer ' + token}

        resp = requests.get(url, headers=header)
        api.print_response(resp)

        assert resp.status_code == 200, "The Item with obID=" + str(obId) + " does not exist. Response Status Code: " \
                                        + str(resp.status_code)

        # DELETE REQUEST

        url = prop.get_apiurl() + SERVOCE_OBS_BLICK + str(obId)  # + '/findingCharts/{n}'

        header = {'Authorization': 'Bearer ' + token, 'If-Match': '*'}

        resp = requests.delete(url, headers=header)
        api.print_response(resp)

        assert resp.status_code == 204, "Deleting of Item failed. obId: " + + str(obId) + \
                                        ", Response Status Code" + str(resp.status_code)

        # GET: Check if the OB was deleted

        url = prop.get_apiurl() + SERVOCE_OBS_BLICK + str(obId)

        header = {'Authorization': 'Bearer ' + token}

        resp = requests.get(url, headers=header)
        api.print_response(resp)

        assert resp.status_code != 200, "Item was not deleted successfully. Response Status Code: " + str(
            resp.status_code)

