import pytest
from libselenium import page
import properties as prop


class Test_POC():

    def setup_method(self, test_method):
        # Runs before each test

        # Gets the given browser and driver in the config file
        self.driver = prop.get_driver()

        # Navigates to the given url in the config file
        self.driver.get(prop.get_weburl())

    def teardown_method(self, test_method):
        # Runs after each test.

        # Closes the browsers
        self.driver.close()

    def test_search_in_python_org(self):
        """
        checks if search function with the given search criteria meets the requirements.
        fails if no row containing the uid in result table appears
        """
        testdata = prop.get_testdata_uid()

        # Loads the main page with 5 sec delay to be loaded completely
        main_page = page.MainPage(self.driver)

        # Hover the mouse on the search menu to get the searching box
        main_page.hover_to_search_menu()

        # Sets the text box to the given uid
        main_page.search_text_element = testdata

        # fails if no row containing the uid appears in the result table
        assert main_page.check_element_available(search_id=testdata), "No results found."


